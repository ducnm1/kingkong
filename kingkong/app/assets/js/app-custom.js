"use strict";
if (typeof jQuery === 'undefined') {
    throw new Error('App requires jQuery')
};

var App = function($) {    
    var combosCarousel = function() {
        $(".combos-carousel").owlCarousel({
            singleItem: true,
            slideSpeed: 500,
            navigation: false,
            pagination: true,
        });
    }
    var gmap = function(){
        var map = new GMaps({
            el: '.gmap',
            lat: -12.043333,
            lng: -77.028333,
            zoomControl : true,
            zoomControlOpt: {
                style : 'SMALL',
                position: 'TOP_LEFT'
            },
            panControl : true,
            streetViewControl : true,
            mapTypeControl: true,
            overviewMapControl: true
        });
        map.addMarker({
          lat: -12.043333,
          lng: -77.028333,
          title: 'Lima',
          click: function(){
            console.log(1)
          },
          infoWindow: {
              content: '<p class="gmap-info-marker">Clementi Mall FoodFare <br/>3155 commonwealth avenue west,<br/>Level 4<br/>Stall No. 10<br/>Singapore 129588</p>'
            }
        });
    }
    var heightWidget = function(){
        if ($(window).width() >=1200) {
            $('.widget-posts-langs').each(function(){
                var max = 0;
                var parent = $(this);
                var titleH = parent.find('.post-title').outerHeight();
                parent.find('.item').each(function(){
                    var child = $(this);         
                    if (child.height() > max) {
                        max = child.height();
                    }
                });
                parent.find('.post-content').css('height', max - titleH);
            });
        }
    }

    var scrollToDiv = function() {
        $(".widget-menus .widget-content").click(function(e) { 
            console.log(1);
            e.preventDefault(); 
            var id = $(this).data("id");
            $('html,body').animate({
                scrollTop: $("[data-goal =" + id + "]").offset().top},
                'slow');

        });
    }
    return {
        init: function() {
            combosCarousel();            
            heightWidget();
            scrollToDiv();
            gmap();
        }
    };
}(jQuery);
$(document).ready(function() {
    App.init();
});