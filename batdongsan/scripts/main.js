/**
 * Created by ducnm on 9/15/2016.
 */
"use strict";
var App = function($){
    var activeLabelSearch = function(){
        $('.custom-tabs .label').click(function(){
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var label = $(this).data('label');
            $(this).closest('.custom-tabs').find('[data-content="'+ label +'"]').addClass('active').siblings().removeClass('active');
            gmap();
        });
    }
    var windowWidth = $(window).width();
    var handleMainMenu = function(){
        if (windowWidth <= 1024) {
            $('#menu_top').addClass('hidden');
            $('nav#menu').removeClass('hidden').mmenu();
        }
    };

    var handleSignleCarousel = function(){
        if ($('.myGallery.owl-carousel').length) {
            $(".myGallery.owl-carousel").owlCarousel({
                singleItem: true,
                autoHeight: true
            });
        }
    }
    var gmap = function(){
        var $el = $('.gmap');
        if ($el.length) {
            var $lat = $el.data('lat');
            var $lng = $el.data('lng');
            var $title = $el.data('title');

            $el.removeAttr('style').html('');

            var map = new GMaps({
                el: '.gmap',
                lat: $lat, 
                lng: $lng,
                zoomControl : true,
                zoomControlOpt: {
                    style : 'SMALL',
                    position: 'TOP_LEFT'
                },
                panControl : true,
                streetViewControl : true,
                mapTypeControl: true,
                overviewMapControl: true
            });
            map.addMarker({
              lat: $lat,
              lng: $lng,
              title: $title
            });
        }
    }
    return {
        init: function(){
            activeLabelSearch();
            handleMainMenu();
            handleSignleCarousel();
        }
    };
}(jQuery);
$(document).ready(function(){
    App.init();
});