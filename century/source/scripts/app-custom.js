if (typeof jQuery === 'undefined') {
  throw new Error('App requires jQuery')
};

var App = function($){
	"use strict";
	var windowWidth = $(window).width();
	var handleMainMenu = function(){
		if (windowWidth > 1024) {
			$('#page-header .main-menu').superfish({
				delay:       400,                   
				animation:   {opacity:'show',height:'show'},
				speed:       'fast',               
				autoArrows:  true
			});
		}
		else
		{	
			$('#page-header .main-menu').addClass('hidden');
			$('nav#menu').removeClass('hidden').mmenu();
		}
	};

	var handleHomeCarousel = function(){
		if ($('.widget-home .owl-carousel').length) {
			$(".widget-home .owl-carousel").owlCarousel({
				singleItem: true,
				autoHeight: true
			});
		}
	}


	return {
		init: function(){
			handleMainMenu();
			handleHomeCarousel();
		}
	};
}(jQuery);
$(document).ready(function(){
	App.init();
});