var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var swig        = require('gulp-swig');
var include     = require("gulp-include");
var uglify      = require('gulp-uglify');
var uglifycss   = require('gulp-uglifycss');
var imageop     = require('gulp-image-optimization');

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'templates', 'scripts'], function() {

    browserSync.init({
        server: "./app"
    });

    gulp.watch("source/scss/**/*.scss", ['sass']);
    gulp.watch("source/templates/**/*.html", ['templates']);
    gulp.watch("source/scripts/*.js", ['scripts']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("source/scss/**/*.scss")
        .pipe(sass())
        // .pipe(uglifycss({
        //   "uglyComments": true
        // }))
        .pipe(gulp.dest("app/assets/css"))
        .pipe(browserSync.stream());
});

// Compile script
gulp.task("scripts", function() { 
    return gulp.src("source/scripts/app-custom.js")
    .pipe(include())
    .pipe(gulp.dest("app/assets/js"));
});
gulp.task("compressjs", function() { 
    return gulp.src("source/scripts/*.js")
    .pipe(include())
    .pipe(uglify())
    .pipe(gulp.dest("app/assets/js"));
});


// Compile html
gulp.task('templates', function() {
    gulp.src('source/templates/pages/*.html')
    .pipe(swig({
      defaults: { cache: false }
    }))
    .pipe(gulp.dest('app/'))
    .pipe(browserSync.reload({stream: true}));
});


// Compress image
gulp.task('images', function(cb) {
    gulp.src(['source/images/**/*.svg','source/images/**/*.png','source/images/**/*.jpg','source/images/**/*.gif','source/images/**/*.jpeg'])
    // .pipe(imageop({
    //     optimizationLevel: 5,
    //     progressive: true,
    //     interlaced: true
    // }))
    .pipe(gulp.dest('app/assets/images')).on('end', cb).on('error', cb);
});

// copy font
gulp.task('fonts', function() {
    gulp.src(['source/fonts/**/*.otf','source/fonts/**/*.eot','source/fonts/**/*.svg','source/fonts/**/*.ttf','source/fonts/**/*.woff','source/fonts/**/*.woff2'])
    .pipe(gulp.dest('app/assets/fonts'));
});


gulp.task('default', ['serve']);